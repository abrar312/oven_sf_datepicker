import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'OverlayEntry SF date picker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  OverlayEntry? overlayEntry;
  LayerLink layerLink = LayerLink();

  OverlayEntry createOverlayEntry() {
    return OverlayEntry(builder: (context) {
      return Positioned(
        width: 400.0,
        child: CompositedTransformFollower(
          link: layerLink,
          offset: const Offset(0.0, 65.0),
          child: Material(
              elevation: 5.0,
              child: SfDateRangePicker(
                showTodayButton: true,
                showNavigationArrow: true,
                onSelectionChanged: (selectedDate) {
                  setState(() {
                    controller.text = DateFormat("dd MMM y").format(
                        DateFormat("yyyy-MM-dd")
                            .parse(selectedDate.value.toString()));
                  });
                  focusNode.unfocus();
                },
              )),
        ),
      );
    });
  }

  @override
  void initState() {
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        overlayEntry = createOverlayEntry();
        Overlay.of(context)!.insert(overlayEntry!);
      } else {
        overlayEntry!.remove();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          width: 400.0,
          child: CompositedTransformTarget(
            link: layerLink,
            child: TextFormField(
              readOnly: true,
              focusNode: focusNode,
              controller: controller,
              decoration: const InputDecoration(
                labelText: "Select date",
                hintText: "Select date",
                suffixIcon: Icon(
                  Icons.calendar_today,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
